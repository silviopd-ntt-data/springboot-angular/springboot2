package com.nttdada.springboot2.service;

import com.nttdada.springboot2.entity.Persona;
import com.nttdada.springboot2.reposity.PersonaRespository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PersonaServiceImpl implements IPersonaService {

    @Autowired
    private PersonaRespository personaRespository;

    @Override
    public Persona buscarPersona(Long id) {
        Optional<Persona> persona = personaRespository.findById(id);
        return persona.orElse(null);
    }

    @Override
    public Persona registrarPersona(Persona persona) {
        return personaRespository.save(persona);
    }

    @Override
    public Persona modificarPersona(Long id, Persona persona) {
        Optional<Persona> personaFounded = personaRespository.findById(id);
        if (personaFounded.isPresent()) {
            Persona personaUpdate = new Persona();
            personaUpdate.setId(id);
            personaUpdate.setNombres(persona.getNombres() != null ? persona.getNombres() : personaFounded.get().getNombres());
            personaUpdate.setApellidos(persona.getApellidos() != null ? persona.getApellidos() : personaFounded.get().getApellidos());
            personaUpdate.setEdad(persona.getEdad() != null ? persona.getEdad() : personaFounded.get().getEdad());
            personaUpdate.setFechaNacimiento(persona.getFechaNacimiento() != null ? persona.getFechaNacimiento() : personaFounded.get().getFechaNacimiento());
            return personaRespository.save(personaUpdate);
        }
        return null;
    }

    @Override
    public String eliminarPersona(Long id) {
        Optional<Persona> personaFounded = personaRespository.findById(id);
        if (personaFounded.isPresent()) {
            personaRespository.deleteById(id);
            return "Persona eliminada correctamente.";
        }
        return "Persona no encontrada.";
    }

    @Override
    public List<Persona> listarPorNombre(String nombre) {
        return personaRespository.findByNombres(nombre);
    }

    @Override
    public List<Persona> listarPorParteNombre(String parteNombre) {

        return personaRespository.findByNombresContaining(parteNombre);
    }

    @Override
    public List<Persona> listarPersonas() {
        return personaRespository.listarPersonasNativa(18);
    }
}
