package com.nttdada.springboot2.service;

import com.nttdada.springboot2.entity.Persona;

import java.util.List;

public interface IPersonaService {

    public Persona buscarPersona(Long id);

    public Persona registrarPersona(Persona persona);

    public Persona modificarPersona(Long id, Persona persona);

    public String eliminarPersona(Long id);

    public List<Persona> listarPorNombre(String nombre);

    public List<Persona> listarPorParteNombre(String parteNombre);

    public List<Persona> listarPersonas();
}
