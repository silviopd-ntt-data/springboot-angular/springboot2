package com.nttdada.springboot2.controller;


import com.nttdada.springboot2.entity.Persona;
import com.nttdada.springboot2.service.IPersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping("/persona")
public class PersonaController {
    @Autowired
    private IPersonaService personaService;

    @GetMapping("/buscar/{id}")
    public Persona buscarPersona(@PathVariable("id") Long idpersona) throws ParseException {
        return personaService.buscarPersona(idpersona);
    }

    @PostMapping("/registrar")
    public Persona registrarPersona(@RequestBody Persona persona) {
        return personaService.registrarPersona(persona);
    }

    @PutMapping("/actualizar/{id}")
    public Persona actualizarPersona(@PathVariable("id") Long idPersona, @RequestBody Persona persona) {
        return personaService.modificarPersona(idPersona, persona);
    }

    @DeleteMapping("/eliminar/{id}")
    public String eliminarPersona(@PathVariable("id") Long idPersona) {
        return personaService.eliminarPersona(idPersona);
    }

    @GetMapping("/listarpornombre")
    public List<Persona> listarPorNombre(@RequestParam("nombre") String nombre) {
        return personaService.listarPorNombre(nombre);
    }

    @GetMapping("/listarporpartenombre")
    public List<Persona> listarPorParteNombre(@RequestParam("partenombre") String parteNombre) {


        return personaService.listarPorParteNombre(parteNombre);
    }

    @GetMapping("/listar")
    public List<Persona> listarPersonas() {
        return personaService.listarPersonas();
    }

}
