package com.nttdada.springboot2.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "persona")
@Table(name = "tb_persona")
public class Persona implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "nombre", length = 100)
    private String nombres;

    private String apellidos;

    private Integer edad;

    @Column(name = "fechaNacimiento")
    private Date fechaNacimiento;

    @Transient
    private Double sueldo;

}
