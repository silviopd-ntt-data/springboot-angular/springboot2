package com.nttdada.springboot2.reposity;

import com.nttdada.springboot2.entity.Persona;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonaRespository extends JpaRepository<Persona, Long> {

    List<Persona> findByNombres(@Param("nombre") String nombre);

    List<Persona> findByNombresContaining(@Param("parteNombre") String parteNombre);

    @Query(value = "Select p from persona p where p.edad > :edadBuscar")
    List<Persona> listarPersonas(@Param("edadBuscar") int edad);

    @Query(value = "Select * from tb_persona p where p.edad > :edadBuscar", nativeQuery = true)
    List<Persona> listarPersonasNativa(@Param("edadBuscar") int edad);
}
